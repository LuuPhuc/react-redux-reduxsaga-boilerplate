import React from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const useNotify = ({ message, type }) => {
    toast.success(message)
}

export default useNotify
