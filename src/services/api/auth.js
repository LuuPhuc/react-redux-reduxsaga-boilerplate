import request from '../../libs/request'

export function loginApi(payload) {
    return request.post('/auth/login', payload)
}

export function registerApi(payload) {
    return request.post('/auth/register', payload)
}
