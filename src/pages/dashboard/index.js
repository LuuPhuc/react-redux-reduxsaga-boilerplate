/* eslint-disable react/react-in-jsx-scope */
// Styles
import withStyles from '@material-ui/core/styles/withStyles'
import { Grid } from '@material-ui/core'
import { styles } from './styles'

// Components
import { BoxInfo } from '../../components/base'

const Dashboard = (props) => {
    const { classes } = props

    const renderOverViewInfo = (data) => {
        return (
            <Grid container spacing={3} style={{ marginBottom: 24 }}>
                {(data || []).map((props, index) => (
                    <Grid key={index} item xs={12} sm={12} md={4} lg={4} xl={4}>
                        <BoxInfo {...props} />
                    </Grid>
                ))}
            </Grid>
        )
    }
}

export default withStyles(styles)(Dashboard)