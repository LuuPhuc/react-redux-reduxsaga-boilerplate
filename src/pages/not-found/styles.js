import { radius, sizes } from '../../assets/fonts'
import { colors, shadow } from '../../assets/colors'
import { getShadowStyle } from '../../utils/styleUtils'

export const styles = (theme) => ({
    wrapper: {
        margin: '0 auto',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        width: '90vw',
    },
    formContainer: {
        display: 'flex',
        height: '70vh',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        borderRadius: radius.lg,
        backgroundColor: colors.white,
        ...getShadowStyle({ color: shadow.primary }),
        '& h3': {
            marginTop: 0,
            fontSize: sizes.sm,
        },
    },
    images: {
        width: '50%',
        margin: '0 auto',
    },
})
