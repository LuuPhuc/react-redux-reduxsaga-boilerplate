/* eslint-disable jsx-a11y/alt-text */
import React from 'react'

// Styles
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './styles'

import { CommonButton } from '../../components/base'
import { images } from '../../assets/images'

const NotFound = (props) => {
    const { classes, history } = props

    return (
        <div className={classes.wrapper}>
            <div className={classes.formContainer}>
                <img className={classes.images} src={images.not_found_background.url} />
                <CommonButton
                    size="large"
                    label="Home"
                    color={{
                        background: 'blue_rgba',
                        text: 'white',
                        opacity: 1,
                    }}
                    style={{ marginTop: 32 }}
                    onClick={() => history.replace('/auth/login')}
                />
            </div>
        </div>
    )
}
export default withStyles(styles)(NotFound)
