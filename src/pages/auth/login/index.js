/* eslint-disable jsx-a11y/alt-text */
import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { useFormik } from 'formik'
import * as Yup from 'yup'

// Styles
import withStyles from '@material-ui/core/styles/withStyles'
import Grid from '@material-ui/core/Grid'
import styled from 'styled-components'
import { styles } from './styles'

import { userLogin } from '../../../redux/actions/user'

// Components
import { images } from '../../../assets/images'
import { TextInput, CommonButton, Link, Typography } from '../../../components/base'

// Constants
import { REQUIRED } from '../../../utils/constants'

const LoginSchema = Yup.object().shape({
    email: Yup.string().required(REQUIRED),
    password: Yup.string().required(REQUIRED),
})

const Login = (props) => {
    const { classes, history } = props

    const dispatch = useDispatch()

    const onSubmit = useCallback(
        async (values) => {
            dispatch(userLogin(values))
            history.replace('/dashboard')
        },
        [dispatch, history],
    )

    const { values, handleChange, handleSubmit, errors } = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: LoginSchema,
        onSubmit: onSubmit,
    })

    return (
        <div className={classes.wrapper}>
            <div className={classes.formContainer}>
                <div className={classes.formLogin}>
                    <form onSubmit={handleSubmit}>
                        <h3>Welcome back</h3>
                        <ContainerHorizontal>
                            <Typography variant="subTitle">New to timebee?</Typography>
                            <Link className={classes.signUp} color="primary" to="/signup" bold>
                                Sign up
                            </Link>
                        </ContainerHorizontal>
                        <TextInput
                            id="email"
                            type="email"
                            name="email"
                            onChange={handleChange}
                            value={values.email}
                            inputError={errors.email}
                            placeholder="Your email address"
                        />
                        <TextInput
                            id="password"
                            type="password"
                            name="password"
                            onChange={handleChange}
                            value={values.password}
                            inputError={errors.password}
                            placeholder="Your password"
                        />
                        <CommonButton
                            fullWidth
                            size="large"
                            label="Login"
                            type="submit"
                            color={{
                                background: 'blue_rgba',
                                text: 'white',
                                opacity: 1,
                            }}
                        />
                        <ContainerForgotPassword justify="flex-end" container>
                            <Link className={classes.forgotPassword} color="primary" to="/forgot-password">
                                Forgot your password?
                            </Link>
                        </ContainerForgotPassword>
                        <Grid container>
                            <OtherOption variant="caption">Or login with</OtherOption>
                        </Grid>
                        <GroupAuth spacing={2} container>
                            <Grid xs={4} item>
                                <ButtonAuth>Google</ButtonAuth>
                            </Grid>
                            <Grid xs={4} item>
                                <ButtonAuth>Linked in</ButtonAuth>
                            </Grid>
                            <Grid xs={4} item>
                                <ButtonAuth>Github</ButtonAuth>
                            </Grid>
                        </GroupAuth>
                        <Terms component="div" variant="small">
                            By continue you accept our{' '}
                            <Link className={classes.forgotPassword} color="primary" to="/term" bold>
                                Terms of use
                            </Link>{' '}
                            and{' '}
                            <Link className={classes.forgotPassword} color="primary" to="/policy" bold>
                                Privacy policy
                            </Link>
                        </Terms>
                    </form>
                </div>
                <img className={classes.images} src={images.login_background.url} />
            </div>
        </div>
    )
}

const ContainerHorizontal = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 20px;
`

const ContainerForgotPassword = styled(Grid)`
    margin-top: 20px;
`

const OtherOption = styled(Typography)`
    margin-top: 29px;
    color: rgba(12, 25, 45, 0.57);
`
const GroupAuth = styled(Grid)`

`

const ButtonAuth = styled(CommonButton)`
    width: 100%;
`
const Terms = styled(Typography)`
    margin-top: 25px;
    color: rgba(12, 25, 45, 0.57);

`

export default withStyles(styles)(Login)
