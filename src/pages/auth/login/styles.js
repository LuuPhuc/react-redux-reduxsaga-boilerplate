import { radius, sizes, fonts } from '../../../assets/fonts'
import { colors, shadow } from '../../../assets/colors'
import { getShadowStyle } from '../../../utils/styleUtils'

export const styles = (theme) => ({
    wrapper: {
        fontFamily: 'Avenir Next',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        margin: '0 auto',
    },
    formContainer: {
        display: 'flex',
        height: 'fit-content',
        borderRadius: radius.lg,
        backgroundColor: colors.white,
        ...getShadowStyle({ color: shadow.primary }),
        '& h3': {
            marginTop: 0,
            fontSize: '18px',
            display: 'flex',
            justifyContent: 'center',
        },
    },
    formLogin: {
        display: 'flex',
        alignItems: 'center',
        margin: '0 auto',
        fontFamily: 'Avenir Next',
    },
    images: {
        width: '65%',
        margin: '0 auto',
    },
    // title: {
    //     fontSize: '18px',
    //     display: 'flex',
    //     justifyContent: 'center',
    // },
    forgotPassword: {
        fontSize: '15px',
    },
    signUp: {
        fontSize: '14px',
    },
    google: {
        color: colors.white,
    },
})
