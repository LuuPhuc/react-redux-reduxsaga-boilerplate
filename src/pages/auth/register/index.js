/* eslint-disable jsx-a11y/alt-text */
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'
import * as Yup from 'yup'

//Styles
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './styles'

import { userRegister } from '../../../redux/actions/user'

import { images } from '../../../assets/images'

//Components
import { TextInput, CommonButton } from '../../../components/base'

// Constants
import { REQUIRED } from '../../../utils/constants'

const RegisterSchema = Yup.object().shape({
    name: Yup.string().required(REQUIRED),
    email: Yup.string().required(REQUIRED),
    password: Yup.string().required(REQUIRED),
})

const Register = (props) => {
    const { classes } = props
    const dispatch = useDispatch()

    const onSubmit = async (values) => {
        dispatch(userRegister(values))
    }

    const { values, handleChange, handleSubmit, errors } = useFormik({
        initialValues: {
            name: '',
            email: '',
            password: '',
        },
        validationSchema: RegisterSchema,
        onSubmit: onSubmit,
    })

    return (
        <div className={classes.wrapper}>
            <div className={classes.formContainer}>
                <div className={classes.formRegister}>
                    <form onSubmit={handleSubmit}>
                        <h3>Register</h3>
                        <TextInput
                            id="name"
                            name="name"
                            label="Name"
                            onChange={handleChange}
                            value={values.name}
                            inputError={errors.name}
                        />
                        <TextInput
                            id="email"
                            name="email"
                            label="Email"
                            onChange={handleChange}
                            value={values.email}
                            inputError={errors.email}
                        />
                        <TextInput
                            id="password"
                            type="password"
                            name="password"
                            label="Password"
                            onChange={handleChange}
                            value={values.password}
                            inputError={errors.password}
                        />
                        <CommonButton
                            fullWidth
                            size="large"
                            label="Submit"
                            type="submit"
                            color={{
                                background: 'blue_rgba',
                                text: 'white',
                                opacity: 1,
                            }}
                        />
                    </form>
                </div>
                <img className={classes.images} src={images.register_background.url} />
            </div>
        </div>
    )
}

export default withStyles(styles)(Register)
