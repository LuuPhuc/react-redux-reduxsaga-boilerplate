import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ThemeProvider } from '@material-ui/core/styles'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import App from './App'
import store from './redux/store'
import theme from './assets/theme'

import './index.scss'

ReactDOM.render(
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <ToastContainer
                // position="bottom-right"
                autoClose={3000}
                closeOnClick
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <App />
            <ToastContainer />
        </ThemeProvider>
    </Provider>,
    document.getElementById('root'),
)
