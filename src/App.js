import React from 'react'
import { Routes } from './routers/routes'

function App() {
    return <Routes />
}

export default App
