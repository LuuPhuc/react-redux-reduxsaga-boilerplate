import * as yup from 'yup'

const MESSAGE_VALIDATE_PASSWORD =
    '<strong>Password</strong> must contain a <strong>letter </strong> and  a <strong>number</strong>,  and minimum of 6 characters'

export const loginSchema = yup.object().shape({
    id: yup.string().email().min(6).max(128).required(),
    password: yup
        .string()
        .matches(/^(?=.*[a-zA-Z])(?=.*[0-9]).{6,}$/, MESSAGE_VALIDATE_PASSWORD)
        .max(128)
        .required(MESSAGE_VALIDATE_PASSWORD),
})

export const registerSchema = yup.object().shape({
    first_name: yup.string().min(2).max(128).required(),
    last_name: yup.string().min(2).max(128).required(),
    email: yup.string().email().min(6).max(128).required(),
    password: yup
        .string()
        .matches(/^(?=.*[a-zA-Z])(?=.*[0-9]).{6,}$/, MESSAGE_VALIDATE_PASSWORD)
        .required(MESSAGE_VALIDATE_PASSWORD),
})

export const forgotPasswordSchema = yup.object().shape({
    email: yup.string().email().min(6).max(128).required(),
})
