export function isUndefined(e) {
    switch (e) {
        case 'undefined':
            return true
        case 'NaN':
            return true
        case NaN:
            return true
        case undefined:
            return true
        case '':
            return true
        case null:
            return true
        case 'null':
            return true
        case false:
            return true
        case 'false':
            return true
        case 'Invalid date':
            return true
        default:
            return false
    }
}

export function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        String(email).toLowerCase(),
    )
}

export function isString(value) {
    if (typeof value === 'string') {
        return true
    }
    return false
}

export function isFunction(func) {
    if (typeof func === 'function') {
        return true
    }
    return false
}
