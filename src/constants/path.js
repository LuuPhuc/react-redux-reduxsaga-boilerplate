const PATH = {
    NONE: '/',
    HOME: '/home',
    LOGIN: '/auth/login',
    REGISTER: '/auth/register',
    DASHBOARD: '/dashboard',
}

export default PATH
