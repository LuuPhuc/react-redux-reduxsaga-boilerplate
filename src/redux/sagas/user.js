import { all, put, takeLatest, call } from 'redux-saga/effects'
import { toast } from 'react-toastify'
import { userActionTypes } from '../actions/user'
import { loginApi } from '../../services/api/auth'
import { registerApi } from '../../services/api/auth'


function* userLogin(action) {
    const params = action
    try {
        const payload = yield call(loginApi, params)
        yield put({ type: userActionTypes.USER_LOGIN_SUCCESS, payload })
        yield put({ type: userActionTypes.GET_USER_INFO })
    } catch (error) {
        yield put({ type: userActionTypes.REGISTER_FAILURE, error })
    }
}

function* userRegister(action) {
    const params = action
    try {
        const payload = yield call(registerApi, params)
        yield put({ type: userActionTypes.USER_REGISTER_SUCCESS, payload })
    } catch (error) {
        toast.error(error.error.message)
        yield put({ type: userActionTypes.USER_REGISTER_FAILURE, error })
    }
}

export default function* authSaga() {
    yield all([takeLatest(userActionTypes.USER_LOGIN, userLogin)])
    yield all([takeLatest(userActionTypes.USER_REGISTER, userRegister)])
}
