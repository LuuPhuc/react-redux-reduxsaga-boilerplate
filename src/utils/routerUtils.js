import history from '../helpers/history'

export function routerPush(path) {
    history.push(path)
}

export function routerReplace(path) {
    history.replace(path)
}

export function routerBack() {
    history.goBack()
}

export function routerForward() {
    history.goForward()
}

export function getLocation(field) {
    if (['hash', 'pathname', 'search', 'state'].includes(field)) return history.location[field]
    return history.location
}
