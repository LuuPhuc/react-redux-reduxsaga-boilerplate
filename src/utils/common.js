/**
 * Validate undefined
 * @param {*} e
 */
export function isUndefined(e) {
    switch (e) {
        case 'undefined':
            return true
        case 'NaN':
            return true
        case NaN:
            return true
        case undefined:
            return true
        case '':
            return true
        case null:
            return true
        case 'null':
            return true
        case false:
            return true
        case 'false':
            return true
        case 'Invalid date':
            return true
        default:
            return false
    }
}
export function isFunction(func) {
    if (typeof func === 'function') {
        return true
    }
    return false
}

/**
 * Validate numberic
 * @param {*} str
 */
export function isNumeric(str) {
    if (isUndefined(str)) {
        return false
    }
    if (str.toString().match(/^[0-9]+$/) === null) {
        return false
    }
    return true
}

/**
 * Validate number
 * @param {*} value
 */
export function isNumber(value) {
    if (typeof value === 'number') {
        return true
    }
    if (value && value.toString().split('.') === 2 && typeof parseFloat(value) === 'number') {
        return true
    }
    if (isNumeric(value)) {
        return true
    }
    return false
}