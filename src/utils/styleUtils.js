import { css } from 'glamor'
import { shadow, colors } from '../assets/colors'
import { isFunction } from './common'

/**
 * @param {*} style 
 */
export function getClassName(style) {
    return css(style)
}

/**
 * @param {*} id 
 */
export function getColor(id) {
    return colors[id] || id
}

/**
 * @param {*} param0 
 */
export function getShadowStyle({ color = shadow.base, bgColor, gradient, disable, size, custom, opacity } = {}) {
    if (!color && !custom) {
        return
    }
    const boxShadow = custom || `${size || '0px 0px 7px'} ${opacity ? toRgbA(color, opacity) : color}`
    // const boxShadow = custom || `${size || '0px 0px 4px 1px'} ${opacity ? toRgbA(color, opacity) : color}`;
    const style = { boxShadow: boxShadow, WebkitBoxShadow: boxShadow, MozBoxShadow: boxShadow }
    if (bgColor) {
        if (gradient) {
            let gradientDirection = gradient.direction || 'to bottom'
            style.backgroundImage = `linear-gradient(${gradientDirection}, ${bgColor})`
        } else {
            style.backgroundColor = bgColor
        }
    }
    if (disable) {
        style.opacity = 0.6
    }
    return style
}

/**
 * @param {*} transition 
 */
export function getTransitionStyle(transition) {
    return {
        MozTransition: transition,
        WebkitTransition: transition,
        OTransition: transition,
        transition: transition,
    }
}

/**
 * @param {*} transform 
 */
export function getTransformStyle(transform) {
    return {
        WebkitTransform: transform,
        MsTransform: transform,
        transform: transform,
    }
}

/**
 * @param {*} hex 
 * @param {*} opacity 
 */
export function toRgbA(hex, opacity) {
    let validInput = /^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)
    if (!validInput) {
        if (hex && hex.toString().indexOf('rgb') > -1) {
            let rgbHash = hex.split('(')[1]
            if (rgbHash) {
                rgbHash = rgbHash.split(')')[0]
                if (rgbHash) {
                    rgbHash = rgbHash.split(',')
                    if (rgbHash.length === 3 || rgbHash.length === 4) {
                        let r = rgbHash[0],
                            g = rgbHash[1],
                            b = rgbHash[2]
                        return `rgba(${r},${g},${b},${opacity || rgbHash[3] || 1})`
                    }
                }
            }
        }
        return hex
    }
    let c = hex.substring(1).split('')
    if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]]
    }
    c = '0x' + c.join('')
    return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity || 1})`
}

/**
 * @param {*} option 
 * @param {*} callback 
 */
export function getSvgStyle(option, callback) {
    const isFunc = isFunction(option)
    const { color, key, style } = option && !isFunc ? option : {}
    const selector = `& path[fill="${key || colors.primary}"]`
    const cb = isFunc ? option : callback
    return cb
        ? cb(selector)
        : { [selector]: { fill: color.indexOf('colors.') > -1 ? colors[color.split('.')[1]] : color, ...style } }
}
