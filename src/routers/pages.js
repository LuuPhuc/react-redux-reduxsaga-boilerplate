// Constant
import { PATH } from '../constants'

// Layout
import AuthLayout from '../components/layouts/auth'
import UnAuthLayout from '../components/layouts/unAuth'

// Components
import Login from '../pages/auth/login'
import Register from '../pages/auth/register'
import Dashboard from '../pages/dashboard'

// Public Page
export const publicPages = [
    {
        exact: true,
        path: PATH.LOGIN,
        component: Login,
        layout: UnAuthLayout,
    },
    {
        exact: true,
        path: PATH.REGISTER,
        component: Register,
        layout: UnAuthLayout,
    },
    {
        exact: true,
        path: PATH.HOME,
        component: Login,
        layout: UnAuthLayout,
    },
]

// Private Pages
export const privatePages = [
    {
        exact: false,
        path: PATH.DASHBOARD,
        component: Dashboard,
        layout: AuthLayout,
    },
]
