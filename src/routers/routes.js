import React from 'react'
import { Route, Router, Switch, Redirect } from 'react-router'
import { useSelector, shallowEqual } from 'react-redux'

// Pages
import { publicPages } from './pages'
import history from '../helpers/history'

// Auth
import AuthLayout from '../components/layouts/auth'
import UnAuthLayout from '../components/layouts/unAuth'

export const Routes = () => {
    const { isAuthorized } = useSelector(
        ({ user }) => ({
            isAuthorized: user.userInfo != null,
        }),
        shallowEqual,
    )
    return (
        <Router history={history}>
            <Switch>
                {!isAuthorized && (
                    <Switch>
                        <Redirect exact={true} from="/" to="/auth/login" />
                        {publicPages.map(({ exact, path, component: Comp }, index) => (
                            <Route
                                {...{ exact, path }}
                                key={index}
                                render={(props) => (
                                    <UnAuthLayout>
                                        <Comp {...props} />
                                    </UnAuthLayout>
                                )}
                            />
                        ))}
                    </Switch>
                )}
                {!isAuthorized ? <Redirect to="/auth/login" /> : <AuthLayout />}
            </Switch>
        </Router>
    )
}
