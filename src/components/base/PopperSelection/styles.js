import React, { useCallback, useState } from 'react'

// Styles
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './styles'

// Components
import Popper from '@material-ui/core/Popper'
import Collapse from '@material-ui/core/Collapse'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'

const PopperSelection = ({ classes, children, options, onChange }) => {
    const [open, setOpen] = useState(null)
    const [popperPosition, setPosition] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null)

    const handleOnChange = useCallback(
        (selected) => {
            setOpen(false)
            onChange(selected)
        },
        [setOpen, onChange],
    )

    const onEventClick = useCallback(
        (event) => {
            const { currentTarget } = event
            const pageX = event?.pageX
            const windowWidth = parseInt(window.innerWidth, 10)

            const position = windowWidth - pageX >= 320 ? 'bottom-start' : 'bottom-end'

            setPosition(position)
            setOpen(true)
            if (!anchorEl) setAnchorEl(currentTarget)
        },
        [anchorEl],
    )
}
