// React
import React from 'react'

// Styles
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { Link as LinkRouter } from 'react-router-dom'

const useStyles = makeStyles(
    (theme) => ({
        root: {
            color: (props) => theme.palette[props.color].main,
            fontFamily: (props) => (props.bold ? 'Avenir Next' : 'Avenir Next'),
        },
    }),
    { name: 'Link' },
)

const Link = ({ blank, to, children, bold, color, ...props }) => {
    const classes = useStyles({ bold, color })

    return blank ? (
        <a className={classes.root} href={to} rel="noreferrer" target="_blank">
            {children}
        </a>
    ) : (
        <LinkRouter to={to} {...props}>
            <span className={classes.root}> {children}</span>
        </LinkRouter>
    )
}

Link.propTypes = {
    to: PropTypes.string.isRequired,
    blank: PropTypes.bool,
    bold: PropTypes.bool,
    // color: PropTypes.oneOf(['primary', 'text', 'error', 'warning']),
}

Link.defaultProps = {
    blank: false,
    bold: false,
    color: 'text',
}
export default Link
