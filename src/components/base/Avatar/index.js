// React
import React from 'react'

import { Avatar as MuiAvatar } from '@material-ui/core'

// Styles
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const useStyles = makeStyles(
    () => ({
        root: {
            width: (props) => props.size,
            height: (props) => props.size,
        },
    }),
    {
        name: 'Avatar',
    },
)

const Avatar = React.forwardRef(({ alt, size, children, sizes, src, srcSet, variant, ...restProps }, ref) => {
    const classes = useStyles({ size })

    return (
        <MuiAvatar
            ref={ref}
            alt={alt}
            className={classes.root}
            sizes={sizes}
            src={src}
            srcSet={srcSet}
            variant={variant}
            {...restProps}
        >
            {children}
        </MuiAvatar>
    )
})

Avatar.propTypes = {
    size: PropTypes.number,
    alt: PropTypes.string,
    children: PropTypes.node,
    sizes: PropTypes.string,
    src: PropTypes.string,
    srcSet: PropTypes.string,
    variant: PropTypes.oneOf(['circle', 'rounded', 'square']),
}

Avatar.defaultProps = {
    size: 40,
    alt: null,
    children: null,
    sizes: null,
    src: null,
    srcSet: null,
    variant: 'square',
}

export default Avatar
