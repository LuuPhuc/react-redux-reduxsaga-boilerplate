import { colors } from '../../../assets/colors'
import { fonts, radius } from '../../../assets/fonts'
import { cStyles } from '../../../constants'
import { getTransitionStyle } from '../../../utils/styleUtils'

export const styles = (theme) => ({
    container: {
        border: fonts.none,
        height: 36,
        padding: '8px 16px',
        display: 'flex',
        alignItems: fonts.center,
        justifyContent: fonts.center,
        cursor: 'pointer',
        boxSizing: 'border-box',
        ...getTransitionStyle('all 150ms linear'),
        '&:hover': {
            backgroundColor: colors.hover,
        },
        '&:focus': {
            MozOutlineStyle: fonts.none,
            outline: fonts.none,
        },
        '&:active': {
            MozOutlineStyle: fonts.none,
            outline: fonts.none,
        },
        '& span': {
            margin: 0,
        },
    },
    hasIcon: {
        '& span': {
            margin: '0px 0px 0px 8px !important',
        },
    },
    labelRight: {
        flexDirection: 'row-reverse',
        '& span': {
            margin: '0px 8px 0px 0px !important',
        },
    },
    disable: {
        cursor: 'default',
        opacity: 0.6,
        '&:hover': {
            backgroundColor: fonts.transparent,
        },
    },
    label: {
        ...cStyles.noneUserSelect,
        ...cStyles.textEllipsis,
    },
    fullWidth: {
        flex: 1,
        width: '100%',
    },
    normal: {
        height: 36,
        borderRadius: radius.sm,
    },
    large: {
        height: 44,
        borderRadius: radius.mid,
    },
})
