import React from 'react'
import classNames from 'classnames'

// import Button from '@material-ui/core/Button'

//Style
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './styles'
import { colors, textColors } from '../../../assets/colors'

import { getClassName, getColor, toRgbA } from '../../../utils/styleUtils'

const CommonButton = (props) => {
    const { classes, label, icon, labelRight, color, disable, style, className, onClick, type, fullWidth, size } = props
    const { background, text, opacity } = color || {}
    let _background = background || color
    let _text = text || color || textColors.base

    const baseClass = getClassName({
        backgroundColor: _background ? toRgbA(getColor(_background) || _background, opacity || 0.2) : 'transparent',
        color: getColor(_text) || _text,
    })

    const hoverClass = getClassName({
        ':hover': {
            backgroundColor: toRgbA(getColor(_background) || _background, 0.1),
            color: getColor(_background) || _background || colors.primary,
        },
    })

    // Button Thuan version
    return (
        <button
            className={classNames(
                classes.container,
                classes[size || 'normal'],
                `${baseClass}`,
                `${hoverClass}`,
                className,
                {
                    [classes.hasIcon]: !!icon,
                    [classes.labelRight]: !!labelRight,
                    [classes.disable]: disable,
                    [classes.fullWidth]: fullWidth,
                },
            )}
            {...{ style, ...(!disable && { onClick }) }}
            type={type || 'button'} // submit, reset
        >
            {/* {icon && <ImageViewer src={icon} clickable={!disable} svg={{ color: '#ffff' }} size={16} />} */}
            <span className={classes.label}>{label}</span>
        </button>
    )
}

export default withStyles(styles)(CommonButton)
