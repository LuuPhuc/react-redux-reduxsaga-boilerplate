import { Grid } from '@material-ui/core'
import { CloudUpload as CloudUploadIcon, Delete as DeleteIcon } from '@material-ui/icons'
import React from 'react'

import Button from '.'

export default {
    title: 'Components/Elements/Button',
    component: Button,
}

const Template = (args) => <Button {...args} />

export const Main = Template.bind({})
Main.args = {
    children: 'Button',
    color: 'primary',
    variant: 'contained',
}

export const ContainedButtons = () => (
    <Grid spacing={2} container>
        <Grid item>
            <Button color="primary" variant="contained">
                Primary
            </Button>
        </Grid>
        <Grid item>
            <Button color="secondary" variant="contained">
                Secondary
            </Button>
        </Grid>
        <Grid item>
            <Button variant="contained" disabled>
                Disabled
            </Button>
        </Grid>
    </Grid>
)

export const OutlinedButtons = () => (
    <Grid spacing={2} container>
        <Grid item>
            <Button color="primary" variant="outlined">
                Primary
            </Button>
        </Grid>
        <Grid item>
            <Button color="secondary" variant="outlined">
                Secondary
            </Button>
        </Grid>
        <Grid item>
            <Button variant="outlined" disabled>
                Disabled
            </Button>
        </Grid>
    </Grid>
)

export const ButtonsWithIconsAndLabel = () => (
    <Grid spacing={2} container>
        <Grid item>
            <Button color="primary" startIcon={<CloudUploadIcon />} variant="contained">
                Upload
            </Button>
        </Grid>
        <Grid item>
            <Button color="secondary" startIcon={<DeleteIcon />} variant="contained">
                Delete
            </Button>
        </Grid>
    </Grid>
)

export const LoadingButtons = () => (
    <Grid spacing={2} container>
        <Grid item>
            <Button color="primary" variant="contained" loading>
                Sign In
            </Button>
        </Grid>
        <Grid item>
            <Button color="primary" variant="outlined" loading>
                Sign Out
            </Button>
        </Grid>
    </Grid>
)
