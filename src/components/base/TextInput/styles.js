import { colors, textColors } from '../../../assets/colors'
import { fonts, sizes, radius } from '../../../assets/fonts'
import { getTransitionStyle } from '../../../utils/styleUtils'

export const styles = (theme) => ({
    wrapper: {
        width: '100%',
        marginBottom: 16,
    },
    root: {
        padding: '0 !important',
    },
    input: {
        flex: 1,
        minHeight: 41,
        marginTop: 8,
        padding: '0 24px',
        border: `2px solid ${colors.hover}`,
        borderRadius: radius.mid,
        fontSize: sizes.xs,
        color: textColors.base,
        ...getTransitionStyle('all 150ms linear'),
        '&::-webkit-input-placeholder': {
            color: textColors.primary,
        },
        '&:-ms-input-placeholder': {
            color: textColors.primary,
        },
        '&::placeholder': {
            color: textColors.primary,
        },
        '&:focus': {
            border: `2px solid ${colors.blue}`,
        },
        '&::-webkit-scrollbar': {
            display: fonts.none,
        },
        '&:-ms-scrollbar': {
            color: textColors.primary,
        },
    },
    multiline: {
        // height: 'fit-content',
        padding: '10px 24px',
    },
    disabled: {
        opacity: 0.5,
        cursor: 'default',
    },
    errorOuter: {
        height: sizes.xss,
    },
    errorText: {
        fontSize: sizes.xss,
        color: textColors.error,
        opacity: 0,
        ...getTransitionStyle('all 150ms linear'),
    },
    errorVisible: {
        opacity: 1,
    },
})
