import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const StatusWrapper = styled.div`
    margin-top: 4px;
    display: flex;
    align-items: center;
`
const StatusText = styled.span`
    font-weight: 300;
    font-size: 0.785rem;
    color: #e02020;
    margin-left: 0.4rem;
`
const Status = ({ status, message }) => {
    return (
        <>
            {status && (
                <StatusWrapper>
                    <StatusText status={status}>{message}</StatusText>
                </StatusWrapper>
            )}
        </>
    )
}
Status.propTypes = {
    message: PropTypes.string,
    status: PropTypes.oneOf(['error', 'warning', 'success']),
}
Status.defaultProps = {
    message: null,
    status: null,
}
export default Status
