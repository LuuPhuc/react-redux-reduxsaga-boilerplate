import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import React from 'react'

const useStyles = makeStyles(
    (theme) => ({
        focusDescriptionsWrapper: {
            transition: 'all 0.15s',
            maxHeight: ({ hasFocus }) => (hasFocus ? '100px' : '0'),
            padding: ({ hasFocus }) => (hasFocus ? '0.8125rem 0.8125rem' : `0 0.8125rem`),
            backgroundColor: 'rgb(242, 244, 247)',
            position: 'relative',
            borderTop: ({ hasFocus }) =>
                hasFocus ? `1px  solid  ${theme.palette.grey['300']}` : `0 solid ${theme.palette.grey['300']}`,
        },
        triangle: {
            transition: 'all 0.15s',
            position: 'absolute',
            opacity: ({ hasFocus }) => (hasFocus ? 1 : 0),
            left: '20px',
            top: '-7px',
            width: '13.866px',
            height: '13.866px',
            backgroundColor: 'rgb(242, 244, 247)',
            transform: 'rotate(45deg)',
            borderTop: `1px solid ${theme.palette.grey['300']}`,
            borderLeft: `1px solid ${theme.palette.grey['300']}`,
        },
        descriptionText: {
            transition: 'all 0.15s',
            fontWeight: 400,
            color: theme.palette.primary.main,
            opacity: ({ hasFocus }) => (hasFocus ? 1 : 0),
            margin: 0,
        },
    }),
    { name: 'FocusDescription' },
)

const FocusDescription = ({ description, hasFocus }) => {
    const classes = useStyles({ hasFocus })

    if (!description) {
        return null
    }
    return (
        <Container classes={{ root: classes.focusDescriptionsWrapper }} hasFocus={hasFocus}>
            <span className={classes.triangle} hasFocus={hasFocus} />
            <div
                className={classes.descriptionText}
                dangerouslySetInnerHTML={{ __html: description }}
                hasFocus={hasFocus}
            />
        </Container>
    )
}
FocusDescription.propTypes = {
    description: PropTypes.oneOfType([PropTypes.string, PropTypes.node, PropTypes.func, PropTypes.shape({})]),
    hasFocus: PropTypes.bool,
}
FocusDescription.defaultProps = {
    description: null,
    hasFocus: false,
}
export default FocusDescription
