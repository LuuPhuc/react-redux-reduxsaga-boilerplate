// Library
import React, { forwardRef } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

// Layout
import Input from '@material-ui/core/Input'

// Styles
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './styles'

const TextInput = forwardRef((props, ref) => {
    const { classes, className, id, value, isNumber, multiline, label, inputError, disabled, ...arg } = props
    const isMultiline = typeof multiline === 'number'
    const textInputProps = {
        disabled,
        value: value || '',
        ...(isMultiline && { rows: multiline, multiline: isMultiline }),
        ...(id && { id }),
        ...arg,
    }

    return (
        <div className={classNames(classes.wrapper, { [classes.disabled]: disabled })}>
            {label && <label>{label}</label>}
            <Input
                fullWidth
                disableUnderline
                inputRef={ref}
                classes={{ root: classes.root, input: classNames(classes.input, { [classes.multiline]: isMultiline }) }}
                {...textInputProps}
            />
            <div className={classes.errorOuter}>
                <span className={classNames(classes.errorText, { [classes.errorVisible]: !!inputError && !disabled })}>
                    {inputError}
                </span>
            </div>
        </div>
    )
})

TextInput.defaultProps = {
    name: '',
    value: '',
    placeholder: '',
    type: 'text', //number
    disabled: false,
    classes: {},
    onChange: () => {},
    onBlur: () => {},
    // onEnter: () => { },
    onFocus: () => {},
    onKeyDown: () => {},
}

TextInput.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.any,
    disabled: PropTypes.bool,
    autoFocus: PropTypes.bool,
    placeholder: PropTypes.string,
    multiline: PropTypes.number,
    rows: PropTypes.number,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    // onEnter: PropTypes.func,
    onFocus: PropTypes.func,
    onKeyDown: PropTypes.func,
    customClasses: PropTypes.object,
    isNumber: PropTypes.bool,
    ref: PropTypes.func,
};

export default withStyles(styles)(TextInput)

// Timebee Style
// React
// import React, { useState } from 'react'
// import { useFormContext } from 'react-hook-form'

// // Library
// import _ from 'lodash'
// import classNames from 'classnames'
// import PropTypes from 'prop-types'

// // Layout
// import Grid from '@material-ui/core/Grid'
// // import Input from '@material-ui/core/Input'
// import IconButton from '@material-ui/core/IconButton'
// import VisibilityIcon from '@material-ui/icons/Visibility'
// import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'
// import Label from './label'
// import Status from './status'

// // Style
// import { styles } from './styles'
// import withStyles from '@material-ui/core/styles/withStyles'
// import FocusDescription from './focusDescription'

// const TextInput = ({
//     id,
//     classes,
//     className,
//     label,
//     helperText,
//     onChange,
//     error,
//     type,
//     onBlur,
//     onFocus,
//     ref,
//     ...props
// }) => {
//     const { register } = useFormContext()

//     const [hasFocus, setFocus] = useState(false)
//     const [showPassword, setShowPassword] = useState(false)

//     const handleFocus = (e) => {
//         setFocus(true)
//         if (_.isEqual(typeof onFocus, 'function')) {
//             onFocus(e)
//         }
//     }

//     const handleBlur = (e) => {
//         setFocus(false)
//         if (_.isEqual(typeof onFocus, 'function')) {
//             onBlur(e)
//         }
//     }

//     const togglePasswordVisibility = () => {
//         setShowPassword((prevState) => !prevState)
//     }

//     return (
//         <Grid direction="column" container>
//             <Label htmlFor={id}>{label}</Label>
//             <Grid error={error} status="error" type={type} container>
//                 <input
//                     ref={(el) => {
//                         register(el)
//                         if (ref && el) {
//                             ref.current = el
//                         }
//                     }}
//                     hasLabel={!!label}
//                     name={id}
//                     type={showPassword ? 'text' : type}
//                     onBlur={handleBlur}
//                     onChange={onChange}
//                     onFocus={handleFocus}
//                     {...props}
//                 />
//                 {type && type === 'password' && (
//                     <>
//                         <FocusDescription description={error} hasFocus={hasFocus} />
//                         <div className={classNames(classes.wrapper)}>
//                             <IconButton aria-label="delete" onClick={togglePasswordVisibility}>
//                                 {showPassword ? (
//                                     <VisibilityOffIcon fontSize="small" />
//                                 ) : (
//                                     <VisibilityIcon fontSize="small" />
//                                 )}
//                             </IconButton>
//                         </div>
//                     </>
//                 )}
//             </Grid>
//             {type && type !== 'password' && <Status message={error} status="error" />}
//         </Grid>
//     )
// }

// TextInput.propTypes = {
//     id: PropTypes.string,
//     label: PropTypes.string,
//     onChange: PropTypes.func,
//     onBlur: PropTypes.func,
//     onFocus: PropTypes.func,
//     type: PropTypes.string,
//     disabled: PropTypes.bool,
//     readOnly: PropTypes.bool,
//     variant: PropTypes.oneOf(['Standard', 'filled', 'outlined']),
//     error: PropTypes.string,
//     ref: PropTypes.oneOfType([
//         // Either a function
//         PropTypes.func,
//         // Or the instance of a DOM native element (see the note about SSR)
//         PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
//     ]),
//     helperText: PropTypes.string,
// }

// TextInput.defaultProps = {
//     id: '',
//     label: '',
//     type: 'text',
//     disabled: false,
//     readOnly: false,
//     onBlur: _.noop,
//     onChange: _.noop,
//     onFocus: _.noop,
//     ref: undefined,
//     variant: 'outlined',
//     error: '',
//     helperText: '',
// }

// export default withStyles(styles)(TextInput)
