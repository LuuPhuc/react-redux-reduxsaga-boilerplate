import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div``
const StyledLabel = styled.label`
    font-weight: 400;
    font-size: 1rem;
`
const Label = ({ htmlFor, children, ...props }) => {
    return (
        <Wrapper>
            <StyledLabel htmlForm={htmlFor} {...props}>
                {children}
            </StyledLabel>
        </Wrapper>
    )
}
Label.propTypes = {
    htmlFor: PropTypes.string,
}
Label.defaultProps = {
    htmlFor: '',
}
export default Label
