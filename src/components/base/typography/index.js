import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import TypographyMui from '@material-ui/core/Typography'
import PropTypes from 'prop-types'

const useStyles = makeStyles(
    (theme) => ({
        root: {
            fontFamily: ({ bold }) => (bold ? 'Avenir Next Bold' : 'Avenir Next'),
        },
        header: {
            fontFamily: 'Avenir Next Bold',
            fontSize: '23px',
            lineHeight: '31px',
        },
        error: {
            color: theme.palette.error.main,
            fontSize: '14px',
            lineHeight: '19px',
        },
        caption: {
            color: theme.palette.secondary.main,
            fontSize: '14px',
            lineHeight: '22px',
        },
        small: {
            color: theme.palette.secondary.main,
            fontSize: '14px',
            lineHeight: '19px',
        },
        button: {
            fontSize: '14px',
            lineHeight: '19px',
        },
        subTitle: {
            fontSize: '16px',
            lineHeight: '22px',
        },
        default: {},
    }),
    { name: 'Typography' },
)
// eslint-disable-next-line react/prop-types
const Typography = ({ variant, children, bold, theme, classes, noWrap, gutterBottom, align, ...props }) => {
    const customClasses = useStyles({ bold, theme })
    return (
        <TypographyMui classes={{ root: `${customClasses.root} ${classes} ${customClasses[variant]} ` }} {...props}>
            {children}
        </TypographyMui>
    )
}

Typography.propTypes = {
    variant: PropTypes.oneOf(['header', 'error', 'caption', 'small', 'button', 'default']),
    align: PropTypes.oneOf(['inherit', 'left', 'center', 'right', 'justify']),
    gutterBottom: PropTypes.bool,
    noWrap: PropTypes.bool,
    bold: PropTypes.bool,
}

Typography.defaultProps = {
    variant: 'default',
    align: 'inherit',
    gutterBottom: false,
    noWrap: false,
    bold: false,
}

export default Typography
