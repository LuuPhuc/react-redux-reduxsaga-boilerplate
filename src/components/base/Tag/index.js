// React
import React from 'react'

// Layouts
import Chip from '@material-ui/core/Chip'

// Styles
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(
    () => ({
        root: {
            borderRadius: '0',
            height: 'auto',
            lineHeight: 'normal',
        },
    }),
    { name: 'Tag' },
)

const Tag = React.forwardRef(
    ({ avatar, label, clickable, color, deleteIcon, disabled, icon, onDelete, variant, ...restProps }, ref) => {
        const classes = useStyles()
        return (
            <Chip
                ref={ref}
                avatar={avatar}
                className={classes.root}
                clickable={clickable}
                color={color}
                deleteIcon={deleteIcon}
                disabled={disabled}
                icon={icon}
                label={label}
                variant={variant}
                onDelete={onDelete}
                {...restProps}
            />
        )
    },
)

Tag.propTypes = {
    avatar: PropTypes.element,
    label: PropTypes.node.isRequired,
    clickable: PropTypes.bool,
    color: PropTypes.oneOf(['default', 'primary', 'secondary']),
    deleteIcon: PropTypes.element,
    disabled: PropTypes.bool,
    icon: PropTypes.element,
    onDelete: PropTypes.func,
    variant: PropTypes.oneOf(['default', 'outlined']),
}

Tag.defaultProps = {
    avatar: null,
    clickable: false,
    color: 'default',
    deleteIcon: null,
    disabled: false,
    icon: null,
    onDelete: null,
    variant: 'default',
}

export default Tag
