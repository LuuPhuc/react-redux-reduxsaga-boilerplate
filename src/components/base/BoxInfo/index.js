import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames';

// Styles
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './styles'

const BoxInfo = (props) => {
    const { classes, label, value, color, icon } = props

    return (
        <div className={classes.boxOuter}>
            <div>
                <h4>{value}</h4>
                <p>{label}</p>
            </div>
        </div>
    )
}

BoxInfo.defaultsProps = {
    label: '',
    value: '',
    color: 'primary',
    icon: '',
}

BoxInfo.propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    color: PropTypes.string,
    icon: PropTypes.string,
}

export default withStyles(styles)(BoxInfo)
