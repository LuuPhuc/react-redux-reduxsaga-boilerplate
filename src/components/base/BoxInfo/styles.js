import { colors, shadow } from '../../../assets/colors'
import { sizes, radius } from '../../../assets/fonts'
import { getShadowStyle } from '../../../utils/styleUtils'

export const styles = () => ({
    boxOuter: {
        minHeight: 100,
        maxHeight: 100,
        width: '100%',
        padding: 16,
        borderRadius: radius.md,
        boxSizing: 'border-box',
        backgroundColor: colors.white,
        display: 'flex',
        alignItems: 'center',
        ...getShadowStyle({ color: shadow.primary }),
        '& h4': {
            fontSize: sizes.md,
            margin: 0,
        },
        '& p': {
            margin: '0 0 0px',
            fontSize: sizes.sm,
        },
    },
    iconOuter: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        maxWidth: 'fit-content',
        padding: 16,
        marginRight: 16,
        borderRadius: radius.md,
    },
    primary: {
        backgroundColor: colors.blue_rgba,
        ...getShadowStyle({ color: shadow.blue }),
    },
    cyan: {
        backgroundColor: colors.cyan_rgba,
        ...getShadowStyle({ color: shadow.cyan }),
    },
    coral: {
        backgroundColor: colors.coral_rgba,
        ...getShadowStyle({ color: shadow.coral }),
    },
    purple: {
        backgroundColor: colors.purple_rgba,
        ...getShadowStyle({ color: shadow.purple }),
    },
    red: {
        backgroundColor: colors.red_rgba,
        ...getShadowStyle({ color: shadow.red }),
    },
})
