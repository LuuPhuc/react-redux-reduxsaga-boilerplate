import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import classNames from 'classnames'

// components
import withStyles from '@material-ui/core/styles/withStyles'
import Avatar from '@material-ui/core/Avatar'
import { ImageIcon, PopperSelection } from '../../base'
import { styles } from './styles'

// import { PopperSelection } from '../../base'

// Utils
// import { getLocation, routerReplace } from '../../../utils/RouterUtils'

// import { iconMenuVertical } from '../../../assets/icons'
import { images } from '../../../assets/images'

// Constants
import { PATH } from '../../../constants'

const Sidebar = ({ classes }) => {
    const history = useHistory()
    const dispatch = useDispatch()

    return (
        <div className={classes.sidebarContainer}>
            <div className={classes.companyContainer}>
                <div className={classes.companyInfo}>
                    <Avatar
                        alt=""
                        style={{ width: 32, height: 32 }}
                        variant="rounded"
                        src={images.default_avatar.url}
                    />
                    <span>The Nice Team</span>
                </div>
            </div>
        </div>
    )
}

export default withStyles(styles)(Sidebar)
