import React, { useState, useEffect } from 'react'
import { Route, Switch, Redirect } from 'react-router'

// Pages
import { privatePages } from '../../../routers/pages'
import NotFound from '../../../pages/not-found'

// Style
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './styles'

// Components
import Sidebar from '../sidebar'

const AuthLayout = ({ classes }) => {
    return (
        <div className={classes.wrapper}>
            <Sidebar />
            <div className={classes.container}>
                <Switch>
                    <Redirect exact from="/" to="/dashboard" />
                    <Redirect exact from="/auth/login" to="/dashboard" />
                    {privatePages.map(({ exact, path, component: Comp }, index) => (
                        <Route {...{ exact, path }} key={index} render={(props) => <Comp {...props} />} />
                    ))}
                    <Route component={NotFound} />
                </Switch>
            </div>
        </div>
    )
}

export default withStyles(styles)(AuthLayout)
