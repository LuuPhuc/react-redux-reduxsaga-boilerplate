import React from 'react'

//Style
import withStyles from '@material-ui/core/styles/withStyles'
import { styles } from './styles'

const UnAuthLayout = (props) => {
    const { classes } = props
    return (
        <div className={classes.wrapper}>
            <div className={classes.container}>
                {React.Children.map(props.children, (child, index) => {
                    return React.cloneElement(child, {
                        index,
                        changeButtonStatus: () => {},
                    })
                })}
            </div>
        </div>
    )
}

export default withStyles(styles)(UnAuthLayout)
