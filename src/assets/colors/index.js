export const colors = {
    // White
    white: '#ffffff',
    white_gray: '#f8f8f8',
    white_gray_unable: '#dfdfe4',
    white_commnet_background: '#f2f2f2',
    white_line: '#ededf0',
    white_borer: '#e2e8f0',

    // Gray
    gray: '#fafafa',

    // Black
    black: '#000000',
    black_gray: '#757575',
    black_gray_light: '#bfbfbf',
    black_normal_text: '#4a4a4a',

    // Primary Color
    green: '#169f84',
    green_light: '#29d7b4',
    green_dark: '#118c74',

    // Orange
    orange: '#f15b00',
    orange_light: '#fc9700',
    orange_super_light: '#fed16e',

    // Red
    red: '#eb4141',
    red_danger: '#f44336',

    // Pink
    pink_light: '#fff3f3',

    // Transparent
    transparent: 'transparent',

    //srgb
    blue_rgba: 'rgba(104, 132, 242, 0.7)',
    purple_rgba: 'rgba(194, 107, 212, 0.7)',
    green_rgba: 'rgba(40, 190, 55, 0.7)',
    moss_rgba: 'rgba(83, 131, 51, 0.5)',
    coral_rgba: 'rgba(252, 137, 89, 0.7)',
    red_rgba: 'rgba(210, 77, 87, 0.7)',

    //hover
    hover: '#EAEEFA',
}

export const textColors = {
    white: '#ffffff',
    base: '#414279',
    primary: '#0F1050',
    active: '#496DF5',
    error: '#d24d57',
}

export const shadow = {
    base: 'rgba(87, 88, 215, 0.25)',
    baseHover: 'rgba(87, 88, 215, 0.25)',
    primary: 'rgba(0, 0, 0, 0.1)',
    blue: 'rgba(104, 132, 242, 0.6)',
    purple: 'rgba(194, 107, 212, 0.6)',
    green: 'rgba(40, 190, 55, 0.6)',
    moss: 'rgba(83, 131, 51, 0.6)',
    coral: 'rgba(252, 137, 89, 0.6)',
    red: 'rgba(210, 77, 87, 0.6)',
    // box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
}
