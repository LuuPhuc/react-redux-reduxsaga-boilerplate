// Font
import AvenirNextBoldOTF from '../fonts/AvenirNextLTPro-Bold.otf'
import AvenirNextItOTF from '../fonts/AvenirNextLTPro-It.otf'
import AvenirNextRegularOTF from '../fonts/AvenirNextLTPro-Regular.otf'

import { createMuiTheme } from '@material-ui/core/styles';
import { colors } from '../colors'

const AvenirNext = {
    fontFamily: 'AvenirNext',
    src: `url(${AvenirNextRegularOTF}) format("opentype")`,
}
const AvenirNextIt = {
    fontFamily: 'AvenirNextIt',
    src: `url(${AvenirNextItOTF}) format("opentype")`,
}
const AvenirNextBold = {
    fontFamily: 'AvenirNextBold',
    src: `url(${AvenirNextBoldOTF}) format("opentype")`,
}

const theme = createMuiTheme({
    palette: {
        primary: {
            main: colors.black_gray,
            light: colors.black_gray_light,
            // dark: colors.green_dark,
            contrastText: colors.black,
        },
        secondary: {
            light: '#ff7961',
            main: '#f44336',
            dark: '#ba000d',
            contrastText: colors.black,
        },
    },
    status: {
        danger: colors.red_danger,
    },
    typography: {
        fontFamily: 'AvenirNext',
    },
    shadow: {
        base: 'rgba(87, 88, 215, 0.25)',
        baseHover: 'rgba(87, 88, 215, 0.25)',
        primary: 'rgba(0, 0, 0, 0.1)',
        blue: 'rgba(104, 132, 242, 0.6)',
        purple: 'rgba(194, 107, 212, 0.6)',
        green: 'rgba(40, 190, 55, 0.6)',
        moss: 'rgba(83, 131, 51, 0.6)',
        coral: 'rgba(252, 137, 89, 0.6)',
        red: 'rgba(210, 77, 87, 0.6)',
    },
    radius: { xs: 4, sm: 8, md: 16, lg: 24, xl: 32 },
    overrides: {
        MuiCssBaseline: {
            '@global': {
                body: {
                    WebkitFontSmoothing: 'antialiased',
                    MozOsxFontSmoothing: 'grayscale',
                },
                '@font-face': [AvenirNextBold, AvenirNextIt, AvenirNext],
            },
        },
    },
})

export default theme
