import colors from './colors'
import fonts from './font'
import images from './images'
import theme from './theme'

export default { colors, fonts, images, theme }
