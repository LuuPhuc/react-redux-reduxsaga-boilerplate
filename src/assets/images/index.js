export const images = {
    login_background: {
        url: require('./auth/login.svg'),
    },
    register_background: {
        url: require('./auth/auth.svg'),
    },
    forget_background: {
        url: require('./auth/forget.svg'),
    },
    not_found_background: {
        url: require('./not-found/img-not-found.svg'),
    },
    default_avatar: {
        url: require('./avatar/df-avatar.jpg'),
    }
}

export const imgSizes = {
    icon: '20px',
    avatar: '32px',
    avatarLarge: '48px',
    button: '48px',
}
