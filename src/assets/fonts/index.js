export const fontFamily = 'Baloo Tammudu 2'

export const fonts = {
    h1: 35,
    h2: 28,
    h3: 20,
    h4: 17,
    h5: 14,
    large: 16,
    small: 13,
    bold: 'bold',
    none: 'none',
    center: 'center'
}

export const sizes = {
    xss: '0.6rem',
    xs: '0.8rem',
    sm: '1rem',
    md: '1.5rem',
    lg: '2rem',
    xl: '3rem',

    title: '1.2rem',
    primary: '0.875rem',
}

export const radius = { xs: 4, sm: 8, mid: 12, md: 16, lg: 24, xl: 32 }
